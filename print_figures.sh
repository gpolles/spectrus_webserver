#!/bin/bash

# import the min and max number of domains
min_dom=`head -1 quality_score.dat | awk '{print $1}'`
max_dom=`tail -1 quality_score.dat | awk '{print $1}'`


# set an upper bound for the max number of sequences in the graph
NUM_MAX_LINES=20
if (( ($max_dom-$min_dom) > $NUM_MAX_LINES )); then
  max_dom=`expr $min_dom + $NUM_MAX_LINES - 1`
fi

awk 'BEGIN{print "Q , quality score"} {print $1, ",", $2}' quality_score.dat > quality_score.csv

# import the number of residues
num_res=`cat final_labels_kmed-${min_dom}.dat | wc -l`


# print the quality score figure
#gnuplot <<-XXX_EMBEDDED_SCRIPT_GNUPLOT_111 
gnuplot <<-XXX_EMBEDDED_SCRIPT_GNUPLOT_111 
#######################################################################
set terminal pngcairo size 1200,800 enhanced font 'Verdana,32' linewidth 2
set output 'quality_score.png'

set xlabel "Number of domains, Q" offset 0,1
set xtics scale 0
set xtics offset 0,0.5
set xtics nomirror
set ylabel "Quality score" offset 1.5
set ytics scale 0
set key off

set clip
set grid lt 0

p 'quality_score.dat' w lp pt 7 lw 3 lt rgb "red"

#######################################################################
XXX_EMBEDDED_SCRIPT_GNUPLOT_111


# build the color matrix
rm -rf color_matrix.dat
for (( q=$min_dom; q<=$max_dom; ++q )); do
  awk '{printf("%d ",$2)} END{printf("\n")}' final_labels_kmed-${q}.dat >> color_matrix.dat
  awk '{printf("%d ", 0)} END{printf("\n")}' final_labels_kmed-${q}.dat >> color_matrix.dat
done


# print the color sequence figure
#gnuplot <<-XXX_COLOR_SEQUENCE_SCRIPT
gnuplot <<-XXX_COLOR_SEQUENCE_SCRIPT
#######################################################################
max_dom=${max_dom}
min_dom=${min_dom}
num_res=${num_res}

max(a,b) = (a > b) ? a : b

num_lines=max_dom-min_dom+1

set terminal png size 1200,600 enhanced font 'Verdana,32' linewidth 2
if ( num_lines > 4  ) set terminal png size 1200,(600+50*(num_lines-4)) enhanced font 'Verdana,32' linewidth 2

set output 'color_sequence.png'

# set palette maxcolors 13
set palette defined ( 0 '#FFFFFF',\
                      1 '#A6CEE3',\
                      2 '#1F78B4',\
                      3 '#B2DF8A',\
                      4 '#33A02C',\
                      5 '#FB9A99',\
                      6 '#E31A1C',\
                      7 '#FDBF6F',\
                      8 '#ff7f00',\
                      9 '#cab2d6',\
                      10 '#6a3d9a',\
                      11 '#ffff99',\
                      12 '#b15928')

unset colorbox

set title offset 0,-0.5 "Decomposition into Q domains"

set yrange [(max_dom-min_dom)*2+2 : -1.5]
unset ytics
set  ytics scale 0.3 0
set ytics nomirror

do for [q=0:(max_dom-min_dom)] {
  set ytics add ( sprintf("%d",q+min_dom) 2*q)  
}

set xlabel "Residue index" offset 0,1
set xrange [-10:(num_res+10)]

set ylabel "Number of domains, Q" offset +0.5,0
set xtics offset 0,0.5
set xtics nomirror
set cbrange [0:max(12,max_dom)]
p 'color_matrix.dat' matrix with image title ''

#######################################################################
XXX_COLOR_SEQUENCE_SCRIPT

