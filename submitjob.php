<!DOCTYPE html>
<html>
<head>
<title> Spectrus Webserver </title>
</head>
<body>
<h1> Spectrus Webserver </h1>
<pre>
<?php


if (!isset( $_POST["email"] ) || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) ){
  $_POST["email"] = "";
}


if(!isset($_POST["input_type"])){
    die("Error parsing data, input_type not set.</pre></body></html>");
}

$input_type = "";
if ( $_POST["input_type"] == "pdb_codes" ) $input_type = "pdb_codes";
if ( $_POST["input_type"] == "file" ) $input_type = "file";

if ($input_type == "") {
    die("Error parsing data, input_type not valid.</pre></body></html>");    
}


$now = time();
$salt = 133422;

$jobid = md5(basename($_FILES["zipfile"]["name"]) . $_POST["email"] . $salt. $now);
$target_dir = $jobid;


mkdir($jobid);


if ($input_type == "pdb_codes") {
  
  $codes = preg_replace("/[^A-Za-z0-9\, ]/", '', $_POST["pdb_codes"]);

// get the pdb from codes and produce a zip
  $cmd_pdb = "bash getentry.sh "."\"".$codes."\" > ". $target_dir ."/getentry.out" ;
  exec($cmd_pdb,$output,$return_var);
  
  // show the output from run_job.sh and if there are errors, exit
  readfile($target_dir."/getentry.out");
  if ( $return_var != 0 ) {
    die("Failed to execute getentry.</pre></body></html>");
  }

  exec("mv zip_from_codes.zip ". $target_dir ."/input_file.zip");  
  
  $fileType = "zip";

} 

if ($input_type == "file") { 

// upload zip/xyz file

    $fileType = pathinfo($_FILES["zipfile"]["name"],PATHINFO_EXTENSION);

    // Allow certain file formats
    if($fileType != "zip" && $fileType != "xyz") 
        die("Error: only .zip or .xyz files are allowed.</pre></body></html>");
    

    $target_file = $target_dir . "/input_file." . $fileType ;

    // Check if file already exists
    if (file_exists($target_file)) 
        die( "Error: file already exists.</pre></body></html>" );
    

    // Check file size
    if ($_FILES["zipfile"]["size"] > 50000000) 
        die("Error: file is too large (>50MB).</pre></body></html>");
    

  // if everything is ok, try to upload file
  
    if ( ! move_uploaded_file($_FILES["zipfile"]["tmp_name"], $target_file) ) 
        die("Sorry, there was an error uploading your file.</pre></body></html>");
    

}

//remove the job folder after some time
$rem_cmd="echo \"rm -rf ".$target_dir."\" | at now +24 hours" ;
$now = time();
exec($rem_cmd);

// save email for the job
$emailfile = fopen($target_dir."/email.txt", "w");
fwrite( $emailfile, $_POST["email"] );
fclose($emailfile);

// save time of deletion
$datefile = fopen($target_dir."/date.txt", "w");
fwrite( $datefile,  date('d M Y H:i', $now+86400 ) );
fclose($datefile);

$res_page = "results.php?id=".$jobid;

// generate input file
$qmin = intval($_POST["qmin"]);
$qmax = intval($_POST["qmax"]);
$kmediter = intval($_POST["kmediter"]);
$cutoff = floatval($_POST["cutoff"]);

// check values provided
if ($qmin<2 || $qmax > 100 || $qmin>$qmax){
    die("Illegal values for qmin (".$qmin.") and qmax (".$qmax.") provided");
}
if ($kmediter<1 || $kmediter > 2000){
    die("Illegal value for kmediter (".$kmediter.") provided");    
}
if ($cutoff<0.1 || $cutoff > 100.0){
    die("Illegal value for cutoff (".$cutoff.") provided");    
}
$inputfile = fopen($target_dir."/INPUT_PARAMS.DAT", "w");
fputs($inputfile,"N_DOM_MIN ".$qmin."\n");
fputs($inputfile,"N_DOM_MAX ".$qmax."\n");
fputs($inputfile,"KMED_ITER ".$kmediter."\n");
fputs($inputfile,"CUTOFF ".$cutoff."\n");
fclose($inputfile);

// start the job
chdir($target_dir);

// il ridirigere l'output è indispensabile perchè il comando sia non-blocking.
$command = "bash ../run_job.sh " . "input_file." . $fileType . " > /dev/null 2>&1 &"; # . " > /dev/null 2>&1 &" ;
exec($command);



chdir("..");

// go to the results page
echo "<p> Your job id is: <b>".$jobid."</b></p>\n";
echo "<p> If you are not automatically redirected to the results page click here: <a href=\"".$res_page."\"> ".
     $res_page."</a></p>";

echo "<script type=\"text/javascript\">
  window.location.href = \"".$res_page."\"
</script>";

?> 
</pre>

</body>
</html>
