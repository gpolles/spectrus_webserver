<!DOCTYPE html>
<?php
$invalid_job = True;
$jobid="";
if (isset($_GET["id"])) {
    $jobid = $_GET["id"];
    if ( preg_match('/[a-z]+[0-9]+/', $jobid ) ) {
        if (is_dir($jobid) ) {
            chdir($jobid);
            $invalid_job = False;
        }
    }
}



?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="SHORTCUT ICON" href="spec_icon.png">
  
<title> Spectrus Webserver results</title>
<meta charset="utf-8">

<?php 
if (!$invalid_job) {
    
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="JSmol.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/data.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript">
<?php
 echo "var jobid=\"".$jobid."\";" ; 
?>

var qmin=2;


function gen_qchart() {

    console.log('hs called');
    // Get the CSV and create the chart
    $.get( jobid + '/results/quality_score.csv', function (csv) {
        console.log('hs getJSON' + csv);
        $('#quality_score').highcharts({

            chart: {
                height: 300
            },

            data: {
                csv: csv
            },
            
            legend: {
                enabled: false
            },

            title: {
                text: 'Quality Score'
            },

            xAxis: {
                allowDecimals: false,
                title: {
                    text: "Number of subdivisions, Q"
                },
                tickWidth: 0,
                gridLineWidth: 1,
            },

            yAxis: { // left y axis
                title: {
                    text: null
                }
            }, 

            tooltip: {
                headerFormat: '<span style="font-size: 10px">Q = {point.key}</span><br/>',
                shared: true,
                crosshairs: true
            },

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                change_subdivision( this.x );
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            },

            series: [{
                name: 'Quality score',
                lineWidth: 4,
                marker: {
                    radius: 4
                }
            }]
        });
    })
    .fail( function() {
        alert('fail');
    });

}



Jmol._isAsync = false;

var jmolApplet0; // set up in HTML table, below

var s = document.location.search;

Jmol._debugCode = (s.indexOf("debugcode") >= 0);


var Info = {
    width: '100%',
    height: 300,
    debug: false,
    color: "0xFFFFFF",
    addSelectionOptions: false,
    use: "HTML5",   // JAVA HTML5 WEBGL are all options
    j2sPath: "./j2s", // this needs to point to where the j2s directory is.
    jarPath: "./java",// this needs to point to where the java directory is.
    jarFile: "JmolAppletSigned.jar",
    isSigned: true,
    serverURL: "http://chemapps.stolaf.edu/jmol/jsmol/php/jsmol.php",
    disableJ2SLoadMonitor: true,
  disableInitialConsole: true,
  allowJavaScript: true
}


function change_subdivision(num) {

    
    var q;
    var l = document.getElementById("sublist");
    if ( typeof(num)==='undefined' ){
        q = l.options[l.selectedIndex].value;
    }else{
        q = num;
    }

    var colors=["[xA6CEE3]", "[x1F78B4]", "[xB2DF8A]", "[x33A02C]", "[xFB9A99]", "[xE31A1C]", "[xFDBF6F]", "[xff7f00]", "[xcab2d6]", "[x6a3d9a]", "[xffff99]", "[xb15928]"];
    var myscheme = "color \"myScheme="
    for (i=0; i<q && i<colors.length; i++){
        myscheme+=colors[i] + " ";
    }
    myscheme += "\" ;"
    var scr = "set antialiasDisplay;load " + jobid + "/results/final_clusterization_kmed-" + q + ".pdb;"
            + myscheme
            + " color {*} property temperature \"myScheme\" ; ribbon ON";
    Jmol.script(jmolApplet0,scr);
    l.value = q;

    var chart = $('#quality_score').highcharts();
    if(chart){
        chart.series[0].data.forEach(function(el){
            el.update({
                marker:{
                    radius: 4,
                    fillColor: '#74A7E4',  
                    states: {
                                hover: {
                                    fillColor: '#74A7E4',
                                    lineColor: 'none'                                    
                                }
                            }
                }
            });
        });
        chart.series[0].data[q-qmin].update({
            marker:{
                radius: 6,
                fillColor: 'red',
                states: {
                    hover: {
                            fillColor: 'red',
                            lineColor: 'red'                                    
                        }
                    }
            }
        } );
    }

}

var job_status = "";
var spec_log = "";
var spec_err = "";

// job steps
var JST_PREPARING = 1;
var JST_ANALYSIS = 2
var JST_DIAGONALIZING = 3;
var JST_CLUSTERING = 4;
var JST_FIGURES = 5;
var JST_COMPLETED = 100;
var JST_ABORTED = -1;


function check_status(){
    
    $.get( "get_status.php?id="+ jobid , function(data,status){
        job_status = data;
    });

    var job_step=0;
    if (job_status == "preparing\n") job_step = JST_PREPARING;
    if (job_status == "creating ENM\n") job_step = JST_ANALYSIS;
    if (job_status == "running spectrus\n") job_step = JST_ANALYSIS;
    if (job_status == "completed\n") job_step = JST_COMPLETED;
    if (job_status == "aborted\n") job_step = JST_ABORTED;


    if (job_step == JST_PREPARING) {
        $("#preparingp img").attr("src","loading.gif");
    }else if (job_step > JST_PREPARING){
        $("#preparingp img").attr("src","check-mark.png");
    };

    if (job_step == JST_ANALYSIS) {
        $("#analysisp img").attr("src","loading.gif");
    }else if (job_step > JST_ANALYSIS){
        $("#analysisp img").attr("src","check-mark.png");
    };

    if (job_step == JST_FIGURES) {
        $("#genfigp img").attr("src","loading.gif");
    }else if (job_step > JST_FIGURES){
        $("#genfigp img").attr("src","check-mark.png");
    };

    if (job_step == JST_ABORTED){
      $("#preparingp img").attr("src","abort.png");
      $("#analysisp img").attr("src","abort.png");  
      $("#genfigp img").attr("src","abort.png");  
    }

    $("#statusp").html(job_status);

    

    $.get( jobid + "/spectrus.log", function(data,status){
        spec_log = data;
    })
    .fail(function(){
        spec_log = "could not retrieve log.";
    });
    $.get( jobid + "/spectrus.err", function(data,status){
        spec_err = data;
    })
    .fail(function(){
        spec_err = "could not retrieve error log.";
    });        
    
 //   $("#logdiv").html( spec_log.replace(/(?:\r\n|\r|\n)/g, '<br />') );
 //   $("#errdiv").html( spec_err.replace(/(?:\r\n|\r|\n)/g, '<br />') );
    
    if(spec_log.length == 0) spec_log = "-- empty log --";
    if(spec_err.length == 0) spec_err = "-- empty log --";

    $("#logdiv").html( spec_log );
    $("#logdiv").scrollTop( $("#logdiv")[0].scrollHeight );

    $("#errdiv").html( spec_err );
    $("#errdiv").scrollTop( $("#errdiv")[0].scrollHeight );

    if (job_step != JST_COMPLETED && job_step != JST_ABORTED){
        setTimeout(check_status,1000);
    }

    // when completed, sets up the visualization using jsmol
    if (job_step == JST_COMPLETED) {
        $("#resultsdiv").show();
        $("#graph_subdivisions").html('<img src="' + jobid + '/results/color_sequence.png" />');
        gen_qchart();
        //$("#quality_score").html('<img src="' + jobid + '/results/quality_score.png" />');
        $("#appdiv").html(Jmol.getAppletHtml("jmolApplet0", Info));
        change_subdivision();
    };

}

$(document).ready(function() {
    $('.masterTooltip').hover(function(){
        // Hover over code
        var title = $(this).attr('title');
        $(this).data('tipText', title).removeAttr('title');
        $('<p class="tooltip"></p>')
        .text(title)
        .appendTo('body')
        .fadeIn('fast');
        }, function() {
        // Hover out code
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
        }).mousemove(function(e) {
        var mousex = e.pageX + 20; //Get X coordinates
        var mousey = e.pageY + 10; //Get Y coordinates
        $('.tooltip')
        .css({ top: mousey, left: mousex })
    });


    var job_status = "";
    check_status();
})

var lastPrompt=0;

function toggle_log(){
    var e = document.getElementById("logdiv");
    var a = document.getElementById("logswitch");

    if (e.style.display == 'block'){
        e.style.display = 'none';
        a.innerText= "+ Show log";
	a.textContent = "+ Show log";
    }else{
        e.style.display = 'block';
        a.innerText = "- Hide log";
	a.textContent = "- Hide log";
    }
}

function toggle_err(){
    var e = document.getElementById("errdiv");
    var a = document.getElementById("errswitch");
    if (e.style.display == 'block'){
        e.style.display = 'none';
        a.innerText = "+ Show error log";
	a.textContent = "+ Show error log";
    }else{
        e.style.display = 'block';
        a.innerText = "- Hide error log";
	a.textContent = "- Hide error log";
    }
}








</script>

<?php

} // invalid job
?> 
</head>
<body>
    <div id="wrapper">
    <div id="logoimgdiv"><img id="logoimg" src="navab_spectrus_black_thin.jpg"></div>
    <div id="content_box">
<h2> Job info: </h2>

<?php if ($invalid_job) {
    echo "Not a valid job id. Your job might be expired and its contents might have been deleted.";
    echo '<hr class="endhr"><div id="footer">page design by Guido Polles (guido.polles@gmail.com)</div></div></div></body></html>';
    die();
}

?>

<p> Job ID: <b>
<?php
 echo $jobid;
?>
</b></p>
<p> Note: Files and results for this job are available for 24 hours (until 
<?php 
    $fhandle = fopen("date.txt", "r");
    $enddate = fgets($fhandle);
    fclose($fhandle);
    echo $enddate;
?>
). You can access them on this page:
<?php
    echo "<a href=\"results.php?id=".$jobid."\">results.php?id=".$jobid."</a>";
?>
</p>

<!-- <h2> Job input parameters: </h2>
<center>
<table class="inptable"> -->
<?php
$qmin=2;
$qmax=2;
    $handle = fopen("INPUT_PARAMS.DAT", "r");
    while (($line = fgets($handle)) !== false) {
        $v = preg_split('/\s+/', $line);
        //echo '<tr><td style="padding-right:30px">'.$v[0]."</td><td style=\"text-align:right\">".$v[1]."</td></tr>\n";
        if (strpos($line,"N_DOM_MAX") !== false){
            $qmax = intval($v[1]);
        }
        if (strpos($line,"N_DOM_MIN") !== false){
            $qmin = intval($v[1]);
        }
    }
    fclose($handle);
    echo '<script type="text/javascript">qmin='.$qmin.';</script>';

?>

<!--
</table>
</center>
-->

<div id="progressdiv">

    <h2> Job progress: </h2>
        <p> Current job status: <span id="statusp">Waiting for data</span></p> 
        

    <p id="preparingp"><img src="redplay.png" class="progress_icon"/>Preparing files</p>
    <p id="analysisp"><img src="redplay.png" class="progress_icon"/>Running analysis</p>
    <p id="genfigp"><img src="redplay.png" class="progress_icon"/>Generating output</p>

</div>


<div id="resultsdiv" style="display:none">
    <h2> Job results: </h2>
    <div id="graphs_container">  

        <div class="figures_container">
            <div class="resultimg" id="quality_score"></div>
            <div id="jm_container">
                <div id="appdiv"></div>
                <center>
                    Select number of subdivisions: 
                    <select id="sublist" onchange="javascript:change_subdivision()">
                    <?php
                        for ($i=$qmin; $i <= $qmax ; $i++) { 
                            echo '<option value="'.$i.'">'.$i.'</option>'."\n";
                        }
                    ?>
                    </select>
                </center>
            </div>
            <div id="graph_subdivisions"></div>    
        </div>

        
    </div>
    <p><?php echo '<a href="'.$jobid.'/results.zip">Click here</a>'; ?> to download all the resulting files and graphs.</p>
</div>



<h2> Job logs: </h2>
<p><a id="logswitch" href="javascript:;" onclick="javascript:toggle_log()">- Hide log</a></p>
<textarea id="logdiv" style="display:block;" cols="80" rows="20" readonly class="logtxa">
</textarea>
<p><a id="errswitch" href="javascript:;" onclick="javascript:toggle_err()">- Hide error log</a></p>
<textarea id="errdiv" style="display:block;" cols="80" rows="20" readonly class="logtxa">
</textarea>
<hr class="endhr">
<div id="footer">2015 - Cristian Micheletti's Group - SISSA</div>
  
</div>
</div>



</body>
</html>
