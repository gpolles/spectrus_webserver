#!/bin/bash

# gets full PDB or required subchain, es. getpdb.x 1aidA
# ver 1.1 11th May 2009, CM
# 

mkdir tmp_dir_getentry
cd tmp_dir_getentry

while IFS='; ,' read -ra ADDR; do
  for pdb_code in ${ADDR[@]}; do

    input=$pdb_code
    pdbname=${input:0:4}
    chain=${input:4:1}

    echo "Fetching pdb file for entry" $pdbname
    outputfile=${pdbname}${chain}.pdb
    rm -f ${pdbname}.ent.gz
    #/opt/local/bin/wget "http://www.rcsb.org/pdb/files/"${pdbname}.pdb.gz -O ${pdbname}.ent.gz >> /dev/null
    wget "http://www.rcsb.org/pdb/files/"${pdbname}.pdb.gz -O ${pdbname}.ent.gz >> /dev/null
    #wget "http://www.rcsb.org/pdb/files/"${pdbname}.pdb.gz -O ${pdbname}.ent.gz >> /dev/null

    gunzip  -f ${pdbname}.ent.gz

    #if chain field non empty then extract corresponding subchain
    if [ -n "$chain" ]; then 
      {
      echo "Extracting subchain " $chain
      cat ${pdbname}.ent | awk -v ch=$chain 'BEGIN{stop_printing=0;found_subchain=0;}{chain=substr($0,22,1);if (($1=="ATOM") && (chain==ch)) found_subchain=1; if ((found_subchain==1) && (($1=="TER") || ($1=="ENDMDL"))) stop_printing=1; if ((stop_printing==0) && ($1=="ATOM") && (chain==ch) && ((substr($0,17,1)==" ")||(substr($0,17,1)=="A")))print}' >  $outputfile
      }
    else
      {
      echo "Considering only first model "
      cat ${pdbname}.ent | awk  'BEGIN{stop_printing=0;}{if ($1=="ENDMDL") stop_printing=1; if ((stop_printing==0) && ($1=="ATOM") && ((substr($0,17,1)==" ")||(substr($0,17,1)=="A")))print}' >  $outputfile
      }
    fi

    #remove downloaded PDB
    rm -f ${pdbname}.ent

    #Let us compute the number of amino acids (only the first model is considered) 
    n_aa=$(cat $outputfile | awk 'BEGIN{stop_counting=0;naa=0;}{type=substr($0,14,2);if ($1=="ENDMDL") stop_counting=1;if ((stop_counting==0) && ($1=="ATOM") && (type=="CA")) naa++;} END{print(naa)}')

    echo "Output written to file " $outputfile " which contains " $n_aa " amino acids."

  done
done <<< "$1"

zip ../zip_from_codes *.pdb

cd ..
rm -rf tmp_dir_getentry

exit 0
