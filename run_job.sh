#!/bin/bash

# make the script exit whenever an error occurs
# set -e

jobid=$(pwd)
jobid=${jobid##*/}

uploaded_file=$1

# find the extension of the loaded file
filename=$(basename "$uploaded_file")
extension="${filename##*.}"

# check for zip or xyz extension

echo "preparing" > status.tmp
if [ "$extension" = "zip" ]; then

  unzip $uploaded_file -d unzipped_pdb_files >> spectrus.log 2>> spectrus.err
  if [ "$?" != 0 ]; then
    echo 'ERROR: '$uploaded_file 'is not a valid zip file.' >> spectrus.err
    echo `date` > done.all
    echo "aborted" > status.tmp
    exit 1
  fi

  num_file=0
  k_old=0

  cd unzipped_pdb_files

  num_pdb_files=`ls -1 *.pdb | wc -l`

  if [ $num_pdb_files -eq 1 ]; then
    # generate trajectory from ENM
    echo "creating ENM" > ../status.tmp

    file=`ls *.pdb`
    echo "$file" > PDB_FILES.TXT
    cp ../../PARAMS_BETAGM.DAT .

    # run betaGM
#    ../../betaGM_special_contacts >> ../spectrus.log 2>> ../spectrus.err
    ../../betaGM_lowmem_slepc >> ../spectrus.log 2>> ../spectrus.err
    if [ "$?" -ne 0 ]
    then
      echo 'Error while generating the beta Gaussian Model.' >> ../spectrus.err
      echo 'Aborted.' >> ../spectrus.err
      echo "aborted" > ../status.tmp
      echo `date` > ../done.all
      exit 1 
    fi

    # generate a metatrajectory from the normal modes
    ../../ENMmodes2traj results_${file} ${file} 300 >> ../spectrus.log 2>> ../spectrus.err
    if [ "$?" -ne 0 ]
    then
      echo 'Error: unable to generate conformations from ENM.' >> ../spectrus.err
      echo 'Aborted.' >> ../spectrus.err
      echo "aborted" > ../status.tmp
      echo `date` > ../done.all
      exit 1
    fi

    # output
    mv ENM_trajectory.xyz ../trajectory.xyz

    # print the number of residues
    num_res=`grep " CA " ${file} | wc -l`
    echo $num_res > ../num_res.dat
    cd ..

  else 
    for file in $(ls -1 *.pdb); do

      num_file=`expr $num_file + 1`

      grep ' CA ' ${file} > ../${file}.CA

      num_res=`cat ../${file}.CA | wc -l `

      # check if files have the same number of residues
      if [ $num_file -ne 1 ]; then
        if [ $num_res -ne $k_old ]; then
          echo 'ERROR: A different number of residues has been found in file ' $file >> ../spectrus.err
          echo `date` > ../done.all
          echo "aborted" > ../status.tmp
          exit 1
        fi
      fi

      k_old=$num_res

    done
  
  
    cd ..

  
    # print the number of residues
    echo $num_res > num_res.dat

    if [ "$num_file" -le 1 ]
    then
         echo 'At least two files needed' >> spectrus.err
         echo 'Aborted.' >> spectrus.err
         echo "aborted" > status.tmp
         echo `date` > done.all
         exit 1
    fi

    # print the file trajectory.xyz
    cat *.CA > trajectory.CA
    cut -c 31-38 trajectory.CA > x.dat
    cut -c 39-46 trajectory.CA > y.dat
    cut -c 47-54 trajectory.CA > z.dat
    paste x.dat y.dat z.dat > trajectory.xyz

    rm -f *.CA x.dat y.dat z.dat
  fi
elif [ "$extension" = "xyz" ]; then
  
  # keep only C atoms
  grep "C" $uploaded_file | awk '{print $2, $3, $4}' > trajectory.xyz

  # get the number of residues
  num_res=`head -1 input_file.xyz | awk '{print $1}'`

  # get the number of lines
  num_lines=`cat trajectory.xyz | wc -l`

  # check if the number of lines in trajectory file is consistent
  remain=`expr $num_lines % $num_res`
  if [ "$remain" != "0" ]; then
    echo "ERROR: The number of residues found is incorrect." >> spectrus.err
    echo "Be sure that you retained only CA atoms in the trajectory file!" >> spectrus.err
    echo `date` > done.all
    echo "aborted" > status.tmp
    exit 1
  fi

  # compute the number of frames
  num_frames=`expr $num_lines / $num_res`

  echo "Number of residues found: $num_res " >> spectrus.log
  echo "Number of frames found:   $num_frames " >> spectrus.log 
  if [ "$num_frames" -le 1 ]
  then
       echo 'At least two frames needed' >> spectrus.err
       echo 'Aborted.' >> spectrus.err
       echo "aborted" > status.tmp
       echo `date` > done.all
       exit 1
  fi


  echo $num_res > num_res.dat

else
  
  echo 'ERROR: Incorrect file format.' >> spectrus.err
  echo 'Only files with .zip or .xyz extension are accepted!' >> spectrus.err
  echo `date` > done.all
  echo "aborted" > status.tmp
  exit 1

fi

if [ "$num_res" -eq 0 ]; then
    echo 'ERROR: no residues ' >> spectrus.err
    echo `date` > done.all
    echo "aborted" > status.tmp
    exit 1
fi


echo "N_ATOMS $num_res" >> INPUT_PARAMS.DAT

echo "running spectrus" > status.tmp

#../spectrus_macosx64 trajectory.xyz >> spectrus.log 2>> spectrus.err
## NON CAMBIARE QUESTO. PIUTTOSTO COPIATI SU /usr/bin/spectrus la tua versione locale!
spectrus trajectory.xyz >> spectrus.log 2>> spectrus.err

echo "generating output" > status.tmp
if [ -d "./results" ]; then
  cd results
  bash ../../print_figures.sh >> spectrus.log 2>> spectrus.err
  cd ..
fi

zip -r results.zip results

read -r email < email.txt
if [ -n "$email" ]
then
  mail -s "Spectrus webserver - job $jobid" "$email" << EOF
  Dear SPECTRUS user, 
     the job $jobid you submitted is now completed.
  You can inspect and download the results at the page
  http://spectrus.sissa.it/results.php?id=$jobid
  Best regards,
  The SPECTRUS team
EOF

fi

echo "completed" > status.tmp
echo `date` > done.all

exit 0
